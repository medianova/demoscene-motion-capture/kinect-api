﻿# Kinect API

## Installation d'un virtual env

```
sudo apt-get install python3-venv python3-pip
python3 -m venv venv
```

## Démarrer le venv

```
. venv/bin/activate
```


http://127.0.0.1:5000/api/sample/perlin_noise?width=200&height=200&run=true

http://127.0.0.1:5000/api/sample/rect_noise?width=200&height=200&run=true
