import cv2
import sys
import logging as log
import datetime as dt
from time import sleep

from Jeu import Jeu

cascPath = "haarcascade_frontalface_default.xml"
faceCascade = cv2.CascadeClassifier(cascPath)
log.basicConfig(filename='webcam.log',level=log.INFO)

video_capture = cv2.VideoCapture(0)
anterior = 0

while True:
    
    if not video_capture.isOpened():
        print('Impossible d\'allumer la webcam')
        sleep(5)
        pass

    # Capture frame-by-frame
    ret, frame = video_capture.read()
    frame2 = ""

    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    faces = faceCascade.detectMultiScale(
        gray,
        scaleFactor=1.1,
        minNeighbors=5,
        minSize=(30, 30)
    )

    # créer un carré vert autour du ou des visages détectés
    for (x, y, w, h) in faces:
        cv2.rectangle(frame, (x, y), (x+w, y+h), (0, 255, 0), 2)

    if anterior != len(faces):
        anterior = len(faces)
        # len(faces) = nombre de visages détecté 
        Jeu.AfficheJoueur(len(faces))
        log.info("faces: "+str(len(faces))+" at "+str(dt.datetime.now()))

    # Affiche la vidéo
    cv2.imshow('Detection de Visages ! ', frame)

    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

# quand tout est fait on libère la vidéo
video_capture.release()
cv2.destroyAllWindows()