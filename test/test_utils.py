import unittest

from kinect_api.resources.utils import string_to_bool, ErrorInput


class MyTestUtils(unittest.TestCase):
    def test_string_to_bool_true(self):
        inputs = ["True", "true"]
        for i in inputs:
            self.assertTrue(string_to_bool(i))

    def test_string_to_bool_false(self):
        inputs = ["False", "false"]
        for i in inputs:
            self.assertFalse(string_to_bool(i))

    def test_string_to_bool_exception(self):
        inputs = ["ok", "fAlse"]
        for i in inputs:
            with self.assertRaises(ErrorInput):
                string_to_bool(i)


if __name__ == '__main__':
    unittest.main()
