from threading import Thread
from time import sleep

from flask_restful import Resource, reqparse
from p5 import noise, noise_seed

from .utils import *
from .. import socketio
import random


class PerlinNoiseStream(Thread):

    def __init__(self, width: int, height: int, delay: int = 1, nb: int = 1):
        super(PerlinNoiseStream, self).__init__()
        self.width = width
        self.height = height
        self.delay = delay
        self.nb = nb

    def run(self):
        def mapp5(x: int, start1: int, stop1: int, start2: int, stop2: int) -> int:
            val = (x - start1) / (stop1 - start1) * (stop2 - start2) + start2
            return int(val)

        x_inc = 0
        y_inc = 0

        random.seed(99)
        noise_seed(99)
        incs = [{'x': 0, 'y': 0} for _ in range(self.nb)]

        while 1:
            points = []
            for inc in incs:

                x = noise(inc['x'])
                y = noise(inc['y'])

                x = mapp5(x, 0, 1, 0, self.width)
                y = mapp5(y, 0, 1, 0, self.height)

                inc['x'] += random.random() / 100
                inc['y'] += random.random() / 100
                print('x', 'y', 'width', 'height')

                points.append({'x': x, 'y': y})

            socketio.emit('perlin-noise-points', points)
            print(points)
            sleep(self.delay)


class PerlinNoise(Resource):

    def __init__(self):
        self.parser = reqparse.RequestParser()
        self.parser.add_argument('width', type=int, required=True, help="Width of canvas")
        self.parser.add_argument('height', type=int, required=True, help="Height of canvas")
        self.parser.add_argument('run', type=str, required=True, help="Run perlin noise stream")
        self.parser.add_argument('delay', type=float, default=1.0, help="Delay of emit point")
        self.parser.add_argument('nb', type=int, default=1, help="Number of point")

        self.thread: PerlinNoiseStream = None

    def get(self):
        args = self.parser.parse_args()
        width = args['width']
        height = args['height']
        delay = args['delay']
        nb = args['nb']

        running = args['run']
        try:
            running = string_to_bool(running)
        except ErrorInput:
            return {"Response": 400, "Error": "Run argument is not a boolean"}

        if running:
            if self.thread is not None:
                self.thread.join()
            self.thread = PerlinNoiseStream(width, height, delay, nb)
            self.thread.start()
        else:
            if self.thread is None:
                return {"Response": 400, "Error": "Perlin noise thread is not running"}
            else:
                self.thread.join()
            self.thread = None

        return {"Response": 200}
