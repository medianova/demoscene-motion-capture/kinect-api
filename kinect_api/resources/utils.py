class ErrorInput(Exception):
    def __init__(self, message):
        self.message = message


def string_to_bool(string: str) -> bool:
    true_str = ["True", "true"]
    false_str = ["False", "false"]

    if string in true_str:
        return True
    elif string in false_str:
        return False
    else:
        raise ErrorInput("Input is not boolean value")