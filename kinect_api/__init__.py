from flask import Flask
from flask_socketio import SocketIO

socketio = SocketIO(cors_allowed_origins="*")


def create_app():
    app = Flask(__name__)
    app.config['SECRET_KEY'] = 'demoscene-kinect-api'

    # Blueprint
    from .blueprints.default import default_bp
    from .blueprints.sample import sample_bp
    app.register_blueprint(default_bp)
    app.register_blueprint(sample_bp)

    # SocketIO
    from .blueprints.socketio import handle_client_connect_event, handle_client_disconnect_event
    socketio.init_app(app)

    return app
