from flask import Blueprint
from flask_restful import Api

from kinect_api.resources.perlin_noise import PerlinNoise
from kinect_api.resources.rect_noise import RectNoise

sample_bp = Blueprint("sample_bp", __name__, url_prefix="/api/sample")
api = Api(sample_bp)

api.add_resource(PerlinNoise, "/perlin_noise")
api.add_resource(RectNoise, "/rect_noise")
