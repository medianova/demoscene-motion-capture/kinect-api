from flask import Blueprint
from flask import render_template
from flask_cors import CORS

route_bp = Blueprint("route_bp", __name__, static_folder="../static")
CORS(route_bp)


@route_bp.route('/')
def sw():
    return route_bp.send_static_file('sw.js')
