from flask import Blueprint
from flask_cors import CORS

default_bp = Blueprint("default_bp", __name__, static_folder="../static")
CORS(default_bp)


@default_bp.route('/sw.js', methods=['GET'])
def sw():
    return default_bp.send_static_file('sw.js')
