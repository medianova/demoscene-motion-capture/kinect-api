from .. import socketio


@socketio.on('client-connect')
def handle_client_connect_event(json):
    print(json["client"] + " is connected")


@socketio.on('client-disconnect')
def handle_client_disconnect_event(json):
    print(json["client"] + " is disconnected")